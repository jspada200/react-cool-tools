# React cool tools

This is just a simple blog application to illistrate the use of React-query, a custom hook, styled components and nextjs

## Backend

The backend being used is not importent for the demo as it is not what is being learned. It was created by William Vincent as part of his Django for APIs book. It was slightly modified to include the django cors headers package to talk to the react front end.

https://github.com/wsvincent/restapiswithdjango

### Running the backend

```
pipenv install
pipenv run python manage.py makemigrations
pipenv run python manage.py migrate
pipenv run python manage.py runserver
```

More information can be found at the link above

## Frontend

The frontend is the main part of this demo. To run the frontend

```
npm install
npm run dev
```
