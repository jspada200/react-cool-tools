import { useState } from "react";

function useForm(inital = {}) {
  const [inputs, setInputs] = useState(inital);

  function handelChange(e) {
    let { value, name, type } = e.target;
    if (type === "number") {
      value = parseInt(value);
    } else if (type === "file") {
      [value] = e.target.files;
    }
    setInputs({
      // copy the existing state
      ...inputs,
      [name]: value,
    });
  }

  function resetForm() {
    setInputs(inital);
  }

  function clearForm() {
    const blankState = Object.fromEntries(
      Object.entries(inputs).map(([key, value]) => [key, ""])
    );
    setInputs(blankState);
  }

  return {
    inputs,
    handelChange,
    resetForm,
    clearForm,
  };
}

export default useForm;
