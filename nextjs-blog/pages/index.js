import { useQuery } from "react-query";
import axios from "axios";
import { CenteredDiv } from "../components/styles";
import React from "react";
import styled from "styled-components";
import Link from "next/link";

const Post = styled.div`
  display: block;
  background-color: gray;
  border-radius: 10px;
  margin: 1rem;
  padding: 1rem;
`;

export default function Home() {
  // Fetch all the blog posts from our API
  const { isLoading, isError, data, error } = useQuery("posts", () =>
    axios.get("http://127.0.0.1:8000/api/v1/")
  );

  // If we are loading from the API, display a loading spinner
  if (isLoading) return <CenteredDiv>Loading</CenteredDiv>;

  return (
    <div className="container">
      {data.data.map((post) => (
        <Post>
          <h2>{post.title}</h2>
          <p>{post.body}</p>
        </Post>
      ))}
    </div>
  );
}
