import { QueryClient, QueryClientProvider } from "react-query";
import Page from "../components/Page";

function MyApp({ Component, pageProps }) {
  const queryClient = new QueryClient();
  console.log("Hello?");
  return (
    <QueryClientProvider client={queryClient}>
      <Page>
        <Component {...pageProps} />
      </Page>
    </QueryClientProvider>
  );
}

export default MyApp;
