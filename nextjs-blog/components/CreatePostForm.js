import axios from "axios";
import React from "react";
import { useMutation } from "react-query";
import useForm from "../api/useForm";

export default function CreatePostForm() {
  // Create a method to make a new post

  // Display a form to add the post
  const { inputs, handelChange, clearForm, resetForm } = useForm({
    title: "",
    body: "",
  });

  const createMutation = useMutation(({ inputs }) =>
    axios.post("http://127.0.0.1:8000/api/v1/", { author: 1, ...inputs })
  );

  // Handel submission of the form
  const handelSubmit = (e) => {
    e.preventDefault();
    createMutation.mutate({ inputs });
    clearForm();
    console.log(createMutation);
  };
  return (
    <fieldset
      disabled={createMutation.isLoading}
      aria-busy={createMutation.isLoading}
    >
      {createMutation.isSuccess && createMutation.data ? (
        <p>You post {createMutation.data.data.title} has been created!</p>
      ) : null}
      <form onSubmit={handelSubmit}>
        <label htmlFor="title">
          Title
          <input
            type="text"
            id="title"
            name="title"
            value={inputs.title}
            onChange={handelChange}
          />
        </label>
        <label htmlFor="body">
          Post
          <input
            type="text"
            id="body"
            name="body"
            value={inputs.body}
            onChange={handelChange}
          />
        </label>
        <input type="submit" value="Submit" />
      </form>
    </fieldset>
  );
}
