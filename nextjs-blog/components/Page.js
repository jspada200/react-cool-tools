import styled from "styled-components";
import Link from "next/dist/client/link";

const NavBar = styled("ul")`
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
  margin-bottom: 15px;
`;

const NavItem = styled("li")`
  float: left;
  a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
  }
  a:hover {
    background-color: #111;
  }
`;

export default function Page({ children }) {
  return (
    <div>
      <NavBar>
        <NavItem>
          <Link href="/">Posts</Link>
        </NavItem>
        <NavItem>
          <Link href="/create">Create</Link>
        </NavItem>
      </NavBar>
      {children}
    </div>
  );
}
